# 🎭 Associations et clubs

## Hack2G2 📚

Hack2g2 est une association créée par les étudiants de l'IUT de Vannes et de l'ENSIBS pour promouvoir le partage des connaissances majoritairement informatiques. Événements, conférences, lives et ateliers sont organisés par l'association pour les membres. Chaque année, l'association organise la hitchhack, une conférence de plusieurs jours avec de nombreux intervenants.

- [Discord](https://discord.gg/RSJJqE48Jw)
- [Site Web](https://hack2g2.fr/)
- [Twitter](https://twitter.com/Hack2G2)
- [LinkedIn](https://www.linkedin.com/company/hack2g2/)
- [Peertube](https://videos.hack2g2.fr/)

## Club CTF - GCC 🚩

Envie de soulever tous les CTF de France et de Navarre jusqu'au FCSC ? Alors bienvenue au club CTF.
C'est le club rattaché au BDE qui représente l'école lors des compétitions CTF.

- [Twitter](https://twitter.com/gcc_ensibs)
- [Linkedin](https://www.linkedin.com/company/gcc-ensibs/?lipi=urn%3Ali%3Apage%3Ad_flagship3_search_srp_all%3Bp0NhAfYXRw6LiqF0klIrZQ%3D%3D)
